var pg = require('pg');
var uuid = require('uuid');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var conString = "postgres://localhost/i67618";

var client = new pg.Client(conString);

app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());
app.set('json spaces', 2);

app.use('/', express.static(__dirname + '/client'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));

app.get('/student/:id', function(req, res) {
	getStudentById(req.params.id, res);
});

app.post('/student', function(req, res) {
	var id = getNewId();
	var name = req.body.name;
	console.log(id, name);

	insertStudent(id, name, res);
});


function insertStudent(id, name, res) {
	pg.connect(conString, function(err, client, done) {
		if (err) {
			return console.error('error fetching client from pool', err);
		}
		client.query(
			'insert into student (id, name) values ($1, $2)',
			[id, name],
			function(err, result) {
			done();

			if (err) {
				return console.error('error running query', err);
			}

			console.log(result);

			res.json({ count: result.rowCount });
		});
	});
}


function getStudentById(id, res) {
	pg.connect(conString, function(err, client, done) {
		if (err) {
			return console.error('error fetching client from pool', err);
		}
		client.query('SELECT * from student where id = $1', [id], function(err, result) {
			done();

			if (err) {
				return console.error('error running query', err);
			}

			console.log(result.rows);

			res.json(result.rows);
		});
	});
}

function getNewId() {
	return uuid.v4();
}

console.log(getNewId());

var port = 3343;
app.listen(port, function() {
	console.log(`App listening on port ${port}...`);
});
