### Postgres Index & Backup

Review Slides:
https://docs.google.com/presentation/d/1a4NGGf1SjTnIo7vFAdI31SMbkSd8gGUfe-2w3tGrkwg/edit?usp=sharing


#### Project

Grading System

Get app from /w35d5-sharding-backup/app/ (get everything but node_modules/)
`npm install` to get dependencies
Change the conString variable to match your database

If you need help with the postgres driver, we're using this one: https://github.com/brianc/node-postgres

This app is a server-only one, with a database and an API. Use postman to test and save your API endpoints.

#### Database: student, class, and grade tables
Run these commands in postgres (command line or pg-admin) to setup the tables we need for this project.

CREATE TABLE STUDENT
(ID VARCHAR(100) NOT NULL,
NAME VARCHAR(100) NOT NULL,
PRIMARY KEY (ID));

CREATE TABLE CLASS
(ID VARCHAR(100) NOT NULL,
NAME VARCHAR(100) NOT NULL,
PRIMARY KEY (ID));

CREATE TABLE GRADE
(STUDENT_ID VARCHAR(100) NOT NULL,
CLASS_ID VARCHAR(100) NOT NULL,
GRADE VARCHAR(2));

alter table grade drop constraint grade_pkey;

alter table grade add constraint student_class unique (student_id, class_id);

#### API:
All APIs should return JSON.

1. Add student
1. Add class
1. Delete student (by id)
1. Delete class (by id)
1. Add/Edit a student's grade in a class (splitting this into two APIs, one for insert and one for update, would simplify this)
1. Delete a student's grade in a class (by student id & class id)
1. List all students
1. List all classes
1. List all students in a class (by class id)
1. List all students and their grade by class (class id)
1. List all classes for a student (by student id)
1. List all classes and grades for those classes by student id.
1. Search for students by name
1. List all classes sorted by total number of students in each class
1. List all classes for a student (by id) sorted by that student's grade in the class (best to worst)
1. List All classes and the average grade in that class
