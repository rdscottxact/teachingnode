# Sharding & Backup/Restore

Mongo Sharding: https://docs.mongodb.org/manual/core/sharding-introduction/

## Mongo Backup/Restore

#### mongodump & mongorestore
pros & cons: https://docs.mongodb.org/manual/core/backups/

> mongodump and mongorestore are simple and efficient for backing up small MongoDB deployments, for partial backup and restores based on a query, syncing from production to staging or development environments, or changing the storage engine of a standalone.

> However, these tools can be problematic for capturing backups of larger systems, sharded clusters, or replica sets. For alternatives, see MongoDB Cloud Manager Backup or Ops Manager Backup Software.

> #### Data Exclusion

> mongodump only captures the documents in the database in its backup data and does not include index data. mongorestore or mongod must then rebuild the indexes after restoring data.

> #### Performance

> mongodump can adversely affect the performance of the mongod. If your data is larger than system memory, the mongodump will push the working set out of memory.

> If applications modify data while mongodump is creating a backup, mongodump will compete for resources with those applications.

> To mitigate the impact of mongodump on the performance of the replica set, use mongodump to capture backups from a secondary member of a replica set.

See also: mongoimport & mongoexport
https://docs.mongodb.org/manual/reference/program/mongoimport/#bin.mongoimport

#### How to use it mongodump & mongorestore
https://docs.mongodb.org/manual/tutorial/backup-and-restore-tools/

`mongodump`

`mongodump --collection myCollection --db test`

`mongorestore --port <port number> <path to the backup>`

`mongorestore`


## Project
