var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

var ObjectId = require('mongodb').ObjectID;

var url = 'mongodb://localhost:27017/matc';

var movies = [];

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/', express.static(__dirname + '/client'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));

app.get('/api/movies', (req, res) => {
	res.json(movies).end();
});

app.get('/api/searchmovie/:title', (req, res) => {
	console.log('searching', req.params);
});

var port = 3343;
app.listen(port, function() {
	console.log(`App listening on port ${port}...`);
});

function findMovies(db, callback) {
	var cursor = db.collection('movies').find();
	cursor.each(function(err, doc) {
		assert.equal(err, null);
		if (doc != null) {
			movies.push(doc)
		} else {
			callback();
		}
	});
};

MongoClient.connect(url, function(err, db) {
	assert.equal(null, err);
	console.log("Connected to mongo");

	findMovies(db, function() {
		// db.close();
	});

	// db.close();
});
