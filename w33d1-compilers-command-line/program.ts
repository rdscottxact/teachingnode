import { Greeting } from './greeting.ts';

class Program {
	constructor(public greeting: Greeting) {
	}

	greet(...n: Array<number>) {
		n.forEach(i => {
			console.log(i);
		});
		console.log(`
			${this.greeting.text}, ${this.greeting.name}
			Welcome!
			`);
	}
}

let p = new Program({
	text: 'hi',
	name: 'Ryan'
});


p.greet(1,2,3,4,54,3);
