"use strict";
var Program = (function () {
    function Program(greeting) {
        this.greeting = greeting;
    }
    Program.prototype.greet = function () {
        var n = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            n[_i - 0] = arguments[_i];
        }
        n.forEach(function (i) {
            console.log(i);
        });
        console.log("\n\t\t\t" + this.greeting.text + ", " + this.greeting.name + "\n\t\t\tWelcome!\n\t\t\t");
    };
    return Program;
}());
var p = new Program({
    text: 'hi',
    name: 'Ryan'
});
p.greet(1, 2, 3, 4, 54, 3);
