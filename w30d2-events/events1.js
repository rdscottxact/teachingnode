const EventEmitter = require('events');

const emitter = new EventEmitter();

emitter.on('event', (data) => {
	console.log('an event occurred!', data.number, data['name']);
});

emitter.emit('event', {
	number: 123, name: 'ZACH'
});

emitter.emit('event', '1234');
