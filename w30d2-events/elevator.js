'use strict';

const EventEmitter = require('events');
var prompt = require('prompt');
var sfx = require("sfx");

sfx.say('Welcome to your elevator!', 'random');

prompt.start();

var elevator = new EventEmitter();

let currentFloor = 0;

prompt.get(['floorNum'], function(err, result) {
	console.log('Command-line input received:');
	console.log(result.floorNum);
	elevator.emit('floorButtonPushed', result.floorNum);
});

elevator.on('floorButtonPushed', (floorNum) => {
	console.log('floor ' + floorNum + ' button pushed');

	setTimeout(function() {
		elevator.emit('closeDoor', floorNum);
	}, 1000)
});

elevator.on('closeDoor', (floorNum) => {
	console.log('door closing');
	setTimeout(() => {
		console.log('door closed');
		elevator.emit('doorClosed', floorNum);
	}, 1000);
})

elevator.on('doorClosed', (floorNum) => {
	elevator.emit('goToFloor', floorNum);
});

elevator.on('goToFloor', (floorNumber) => {
	console.log('going to floor ', floorNumber);
	setTimeout(() => {
		elevator.emit('arrivedAtFloor', floorNumber);
	}, 1500);
});

elevator.on('arrivedAtFloor', (floorNumber) => {
	console.log('arrived at floor ', floorNumber);
});
