pp slides

**project**

https://docs.mongodb.org/getting-started/node/client/

- Make sure we have a mongo database running that we can access
- Insert 3 documents into a collection
	- { number: 1 }, { number: 2 }, { number: 3 }
- Check that there are 3 documents in our collection
- Update the { number: 3 } document to be { number: 2 }
- Check that there are two documents with the number: 2
- Delete the { number: 1 } document with deleteOne
- Check that there are 2 documents with the number: 2 and a total of 2 documents in the entire collection
- Delete all { number: 2 } documents
- Check that there are no documents in the collection

---

- Create a new collection, but instead of 3 different documents, create a single document that has a single property that is a list; this list will have three items in it.
- Structure will look something like this:

```
{
	items: [
		{ number: 1 },
		{ number: 2 },
		{ number: 3 }
	]
}
```

- Run the same operations on the three items within this list as we did in the first exercise.

---

Convert a project that is using firebase to use your mongo database instead; or create a new project (todo app, whatever) that uses a mongo backend.
