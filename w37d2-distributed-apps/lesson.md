## Distributed apps, AWS, App Engine, Azure

Go over features of App Engine vs Compute Engine vs Container Engine
https://cloud.google.com/


* Scalable (vertical vs horizontal)
* load balancing
* health checks
* application logging
* Traffic splitting (a/b test, incremental rollout)
* Task Queue


* VM
* Persistent Disk Storage
* Clusters
* High CPU vs High Memory (use cases - examples of different internet services.)


* Cluster Manager
* Container
* Replica
* KeepAlive policy

Go over feature lists — make sure we know what everything means

Overview of distributed architecture from user request to server response.

Go over project requirements (final project)

If we have time we can go over Docker and setup docker and deploy a container