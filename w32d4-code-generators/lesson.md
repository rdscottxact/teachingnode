# Code Generators

Yeoman, Slush, Plop

- Prompts
- templates
- Scaffolding a new project


### Projects
Use an existing slush generator to scaffold out a new project.
- Use our-project generator
- Update to ask for the `<title>` element in the HTML; use in template
- Update to ask for the port and IP address of your local server
	- Use these variables in the generator template
	- Update slushfile.js to ask for port and ip
	- Update client.js to use ip and port for socket.io server
	- Update server.js to use port
- Ask to install dependencies

***

#### more node + websockets
Modify our multiplayer game to:
- keep track of players
- remove players if they disconnect
- allow players to enter their name
- display player name next to player
- assign each player a random color
