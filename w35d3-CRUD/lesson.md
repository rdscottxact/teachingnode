### projects

Download app
http://www.postgresql.org/download/macosx/

Download and unzip (double-click) sample database at world-1.0.tar.gz on gitlab

Add to path
http://postgresapp.com/documentation/cli-tools.html

`\i ~/Downloads/dbsamples-0.1/world/world.sql`

`\l` Show Databases

`\dt` Show tables

`\d+ [table name]` View table schema


TODO
- `select`
- `where`
- `insert into`
- `update`
- `delete from`
- select total number of cities
- select total population
	1429559884
- select average population
	350468.223584211817
- select cities with population less than 3000
- select cities with population less than 3000 and greater than a million
- Order cities by population
- Select all cities not in the USA using `except`
- List all cities in the country with the largest surface area.
- Select all city names with country code of 'AUS' and 'BMU', using `or` and `in`.

#### Project
Run sql commands from a NodeJS application.
