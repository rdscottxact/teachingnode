var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);

var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({	extended: true }));
app.use(bodyParser.json());

app.use('/', express.static(__dirname + '/client'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));

app.get('/api/time', function(req, res) {
	res.send({
		time: new Date().getTime()
	});
});

io.on('connection', function(socket) {

	// setInterval(function() {
	//
	// 	socket.emit('time', {
	// 		time: new Date().getTime()
	// 	});
	//
	// }, 1000);
	// socket.on('my other event', function(data) {
	// 	console.log(data);
	// });
	//
	socket.on('mousemove', function(data) {
		console.log('mousemove', data);

		// data.id = socket.id;
		socket.emit('clientmousemove', data);
		socket.broadcast.emit('clientmousemove', data);
	});
});




server.listen(3000, function() {
	console.log('server started');
});
