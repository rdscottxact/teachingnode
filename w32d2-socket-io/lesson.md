# Socket.io

http://socket.io/

- Request/Response vs bi-directional communication
- Uses for both

on (listen)  
emit  
broadcast  

Very much like EventEmitter

### Project
Create a website that displays the current time and updates it from the server via web sockets every second.

Create a website that multiple users can connect to and create an endpoint that will trigger some text to broadcast and display to all websites.

Create a website with an input box that sends text to the server via web socket and the server broadcasts out that text to all listening websites.

Move a circle across the screen, syncing the position with the server and broadcasting to all listeners. (everyone can move the ball)

Complete one of the getting started activities for socket.io: http://socket.io/get-started/
