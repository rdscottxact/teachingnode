'use strict';

// setInterval(function() {
// 	getJson('/api/time').then(data => {
// 		document.getElementById('container').innerHTML = data.time;
// 	});
// }, 1000);
var socket = io.connect('http://10.0.112.163:3000');

var people = {};

document.addEventListener('mousemove', function(event) {
	// console.log(event.x, event.y);
	socket.emit('mousemove', {
		id: socket.id,
		x: event.x,
		y: event.y
	});
});

socket.on('time', function(data) {
	// console.log(data);
	document.getElementById('container').innerHTML = data.time;
	// socket.emit('my other event', {
	// 	my: 'data'
	// });
});

socket.on('clientmousemove', function(data) {
	// console.log('clientmousemove', data);
	people[data.id] = data;

	console.log(data.id);
});
// 10.0.112.163:3000


var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
ctx.fillStyle = "#1100FF";

function draw() {
	ctx.clearRect(0, 0, 400, 400);

	for (var id in people) {
		// console.log(people[id]);
		ctx.fillRect(people[id].x, people[id].y, 10, 10);
	}

	window.requestAnimationFrame(draw);
}

window.requestAnimationFrame(draw);

function getJson(endpoint) {
	return window.fetch(endpoint).then(response => {
		return response.json()
	});
}
