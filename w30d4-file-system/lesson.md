# File system

_What is the purpose of the file system?_

persistent storage vs in-memory
size limitations

https://nodejs.org/api/fs.html

- exists
- readFile
- writeFile
- appendFile
- encoding
- line terminators
- file permissions


### Projects
Read an existing file and print it to the console
 - Check that it exists first

Write a multi-line file with your own content
Read and print contents of that file to verify write

Read contents of one file and append it to another file

Write to a file that you have set to read-only; catch exception

**More**

Create a twitter clone that saves each tweet as its own file (dynamic file naming)

- Write new tweet
	- Write tweet to a new file in the "tweets" directory
	- Name the new file with the current time
- Print out all tweets
	- Requires us to read the contents of a directory and print them all
	- Sort files by date

CodeSchool
