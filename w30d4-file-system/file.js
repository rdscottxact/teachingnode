'use strict';

const fs = require('fs');

function readFile(fileName, callback) {
	fs.stat(fileName, (error, stats) => {

		if (stats !== undefined) {
			// console.log(stats.isFile());

			fs.readFile(fileName, 'utf8', (err, data) => {
				console.log(data); // can also do data.toString() with no encoding
				callback(data);
			});
		} else {
			console.log('File does not exist');
		}

	});
}

Read and parse JSON
readFile('data.json', (data) => {
	console.log(data);
	let json = JSON.parse(data);
	console.log(json.property);
});

// fs.writeFile
let myVar = 'something';
fs.writeFile('newfile.txt',
	`
${myVar}
some new text
	another line
	`,
	(err, written, string) => {
		console.log(err, written, string);
	});

// TODO: Write to read-only file
// chmod -w filename.txt
