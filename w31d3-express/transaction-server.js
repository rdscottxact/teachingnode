var express = require('express');
var app = express();
const fs = require('fs');
var bodyParser = require('body-parser');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
	extended: true
})); // support encoded bodies

var transactions = [];

fs.readFile('mock_data.json', 'utf8', (err, data) => {
	transactions = JSON.parse(data);
	// console.log(transactions);
});

app.get('/transaction/:id', function(req, res) {
	console.log(req.params.id);
	var id = req.params.id;

	var found = false;
	for (var i = 0; i < transactions.length; i++) {
		if (id == transactions[i].id) {
			res.json(transactions[i]);
			//res.next();
		}
	}

	// res.sendStatus(418);
	// res.status(404).end('Not Found');
	res.json({
		message: "Transaction " + id + " not found :("
	}).end();
});

app.post('/transaction/:id', function(req, res) {
	console.log(req.body.data);
	var transaction = req.body.data;

	var id = Math.round(Math.random() * Number.MAX_SAFE_INTEGER);
	transaction.id = id;

	transactions.push(transaction);

	console.log(transactions);

	fs.writeFile('mock_data.json', JSON.stringify(transactions),
		function(err) {
		console.log(err, 'wrote transaction');
	});

	res.json(transaction).end();
});

app.get('/search/:pattern', function(req, res) {
	var regex = new RegExp(req.params.pattern);
	regex.ignoreCase = true;

	// for loop
	var foundItems = [];
	transactions.forEach(item => {
		var search = item.username + item.first_name + item.last_name;
		if (regex.test(search)) {
			foundItems.push(item);
		}

		// if (search.indexOf(req.params.pattern) !== -1) {
		// 	foundItems.push(item);
		// }
	});

	res.json(foundItems);

});

app.listen(3000, function() {
	console.log('Bank Transaction API running on port 3000!');
});
