# Express

Routes
http://expressjs.com/en/guide/routing.html

pattern matching

next()


### Projects
Create API with existing data
- Uses GET, POST, PUT, DELETE
- Request and Response in JSON
- Every response should have something in it
	- Return errors if appropriate
- Get record by ID
- Create record
- Update record by ID
- Delete record by ID
- Is it possible to do the above 4 things with a single endpoint that supports 4 methods?
- Add middleware to log requests
	- What else could we do with middleware?
- Regex search that searches in username, first name and last name for the given substring
- Test all APIs with curl, postman, etc.

Continue work on an express project  
Add express backend to angular app  

codeschool
