## Marketing, Brand Building

https://moz.com/beginners-guide-to-seo

_How do search engines crawl websites? JavaScript apps?_

robots.txt
- user agents
- disallow

http://www.robotstxt.org/db/googlebot.html

How do search engines rank sites?
- Google’s PageRank
- Keywords

keywords vs links

Making content indexable
- alt tags on images
- alternative content for plugin-based content or JavaScript content
	- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/noscript
- nofollow
- meta robots tag (Don't scan me)
- meta description <- search results
- meta keywords

Direct navigation vs referral vs search traffic

Gather information with Google Analytics

Build your own analytics engine in Node
- Record referrers, URLs hit, etc. from request
- Log information in counters