## Documentation

http://devdocs.io/
https://github.com/PharkMillups/beautiful-docs

Basic commenting /\* /\*\* // and their uses

Go over list of available jsdoc annotations
http://usejsdoc.org/

Annotate some existing JavaScript
- namespace
- descriptions
- classes
- constructors
- methods
- parameters
- types
- return types
- html in comments

Install jsdoc

Generate HTML documentation

***

### Markdown / Github Markdown

https://help.github.com/articles/basic-writing-and-formatting-syntax/

https://help.github.com/articles/working-with-advanced-formatting/

http://markdowntutorial.com/


Create a markdown file from scratch to showcase the different markdown elements.
Create a readme for an existing project

***

### Git log as documentation

Commit message format

What to include in a git message
* motivation (what and why)
* ticket #s

Ways to view and search git log

***

### Code formatting as documentation
Clean, self-describing code is its own documentation.
- formatting guidelines
- language choice
- framework choice
- architecture choice

Clean code may eliminate the need to maintain comments