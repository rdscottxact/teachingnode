var express = require('express');
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser');
var expressSession = require('express-session')
var passport = require('passport');
var app = express();
var LocalStrategy = require('passport-local').Strategy;


// ORDER IS IMPORTANT
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressSession({
	secret: 'secret',
	resave: false,
	saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', express.static(__dirname + '/client'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));


app.post('/api/login', function(req, res, next) {
	passport.authenticate('local', function(err, user, info) {
		console.log(err, user, info);
		// req.login() will save user object to session
		req.login(user, (error) => {
			if (error) {
				console.log('error login', error);
				res.end();
			} else {
				console.log('login success', user);
				res.json(user);
			}
		});

	})(req, res, next);
});

app.get('/api/check', function(req, res) {
	console.log('checkUser', req.isAuthenticated(), req.params, req.body);
	res.json(req.user);
});

app.listen(4001, function() {
	console.log('App listening on port 4001...');
});

passport.serializeUser(function(user, done) {
	console.log('serializeUser', user);
	done(null, user);
});

passport.deserializeUser(function(user, done) {
	console.log('deserializeUser', user);
	done(null, user);
});

passport.use(new LocalStrategy(
	function(username, password, done) {
		console.log('logging in', username, password);
		// Don't forget to pass user object to done()
		done(undefined, {
			username: username
		});
	}
));