var express = require('express');
var app = express();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


passport.use(new LocalStrategy(
	function(username, password, done) {
		console.log('logging in', username, password);
		done(null, {
			username: username
		});
	}
));

app.use('/', express.static(__dirname + '/client'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));

app.post('/api/login', function(req, res, next) {
	passport.authenticate('local', function(err, user, info) {
		console.log(err, user, info);
		res.json(user);
	})(req, res, next);
});

app.listen(3000, function() {
	console.log('App listening on port 3000...');
});
