angular.module('app', ['ngMaterial'])

.component('login', {
	templateUrl: 'login.html',
	controller: LoginCtrl,
	bindings: {
		myVariable: '='
	}
});

function LoginCtrl($http) {
	this.username = 'myusername';
	this.password = 'mypassword';
	this.$http = $http;
	this.greeting = 'hi';
}

LoginCtrl.prototype.login = function() {
	console.log('logging in...');
	console.log(this.username + ' ' + this.password);
	this.$http.post('/api/login', {
		username: this.username,
		password: this.password
	}).then(() => {
		console.log('done login');
	}).catch(() => {
		console.log('login error');
	});
}

// TODO: BIND USERNAME & PASSWORD TO VARIABLES
// TODO: PASS THOSE VARIABLES IN THE POST REQUEST
