var express = require('express');
var app = express();
const fs = require('fs');
var bodyParser = require('body-parser');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.get('/', function (req, res) {
  res.send('Hello World again :)');
});

var tweets = [];

app.post('/', function(req, res) {
	tweets.push(req.body.tweet);
	console.log(tweets);
	res.send('You posted ' + req.body.id);
});

app.delete('/', function(req, res) {
	res.send('Delete Request Made\n');
});

app.put('/', function(req, res) {
	res.send('Put Request Made\n');
});

app.get('/file/:fileName', function(req, res) {
	console.log(req.params);
	fs.readFile(req.params.fileName, 'utf8', (err, data) => {
		res.send(data);
	});
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
