# Node.js web server

Overview request and response; GET, POST, etc.; api basics

Overview of routes

Overview of curl; postman

http://expressjs.com/

Create a GET and POST endpoint that returns a string

Curl those endpoints
Postman those endpoints

request object
- params, body, baseurl, hostname, etc.

response object
- header, end

Set status code of response

Inspect requests/response object in chrome devtools.


### Projects
Create endpoint to return current time

Create endpoint that returns the contents of file

Create a post endpoint that writes the post params to a file

Create a twitter clone with one post endpoint to create a tweet and a get endpoint to list all created tweets
 - add a key that has to be passed with the post request to allow the tweet to be created; validate key before tweet creation; return error status if not valid

codeschool
